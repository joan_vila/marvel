package joanvila.com.domain.model

data class Character(
        val id: Long,
        val name: String,
        val description: String,
        val characterThumbnail: String,
        val isFavourite: Boolean
)