package joanvila.com.domain.interactor

import io.reactivex.Completable
import joanvila.com.domain.base.CompletableUseCase
import joanvila.com.domain.model.Character
import joanvila.com.domain.repository.CharactersRepository
import javax.inject.Inject

class AddFavoriteCharacterInteractor @Inject constructor(private val repository: CharactersRepository):
        CompletableUseCase<Character>() {

    override fun buildUseCaseObservable(character: Character): Completable {
        return repository.addFavouriteCharacter(character)
    }
}