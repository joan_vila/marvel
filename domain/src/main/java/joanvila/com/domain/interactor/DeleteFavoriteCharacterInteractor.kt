package joanvila.com.domain.interactor

import io.reactivex.Completable
import joanvila.com.domain.base.CompletableUseCase
import joanvila.com.domain.repository.CharactersRepository
import javax.inject.Inject

class DeleteFavoriteCharacterInteractor @Inject constructor(private val repository: CharactersRepository):
        CompletableUseCase<Long>() {

    override fun buildUseCaseObservable(id: Long): Completable {
        return repository.deleteFavoriteCharacter(id)
    }
}