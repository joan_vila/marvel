package joanvila.com.domain.interactor

import io.reactivex.Single
import joanvila.com.domain.base.SingleUseCase
import joanvila.com.domain.model.Character
import joanvila.com.domain.repository.CharactersRepository
import javax.inject.Inject

class GetCharactersInteractor @Inject constructor(private val repository: CharactersRepository):
        SingleUseCase<Int, List<Character>>() {

    public override fun buildUseCaseObservable(offset: Int?): Single<List<Character>> {
        return repository.getCharacters(offset)
    }
}