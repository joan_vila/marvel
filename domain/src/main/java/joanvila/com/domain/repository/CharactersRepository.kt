package joanvila.com.domain.repository

import io.reactivex.Completable
import io.reactivex.Single
import joanvila.com.domain.model.Character

interface CharactersRepository {
    fun getCharacters(offset: Int?): Single<List<Character>>
    fun addFavouriteCharacter(character: Character): Completable
    fun deleteFavoriteCharacter(id: Long): Completable
}