package joanvila.com.domain.base

import io.reactivex.Single

abstract class SingleUseCase<in Params, T> {

    protected abstract fun buildUseCaseObservable(params: Params? = null): Single<T>

    fun execute(params: Params? = null) = buildUseCaseObservable(params)

}