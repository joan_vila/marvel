package joanvila.com.domain.base

import io.reactivex.Completable

abstract class CompletableUseCase<in Params>  {

    protected abstract fun buildUseCaseObservable(params: Params): Completable

    fun execute(params: Params): Completable {
        return this.buildUseCaseObservable(params)
    }

}