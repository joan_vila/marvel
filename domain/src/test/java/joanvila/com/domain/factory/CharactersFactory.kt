package joanvila.com.domain.factory

import joanvila.com.domain.model.Character
import java.util.concurrent.ThreadLocalRandom

class CharactersFactory {

    companion object Factory {

        fun makeCharactersList(count: Int): List<Character> {
            val characters = mutableListOf<Character>()
            repeat(count) {
                characters.add(makeCharacter())
            }
            return characters
        }

        fun makeCharacter(): Character {
            return Character(
                    randomLong(),
                    randomText(),
                    randomText(),
                    randomText(),
                    false
            )
        }

        fun randomText(): String {
            return java.util.UUID.randomUUID().toString()
        }

        fun randomLong(): Long {
            return ThreadLocalRandom.current().nextInt(0, 10).toLong()
        }
    }


}