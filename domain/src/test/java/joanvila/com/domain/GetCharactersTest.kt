package joanvila.com.domain

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import joanvila.com.domain.factory.CharactersFactory
import joanvila.com.domain.interactor.GetCharactersInteractor
import joanvila.com.domain.model.Character
import joanvila.com.domain.repository.CharactersRepository
import org.junit.Before
import org.junit.Test

class GetCharactersTest {

    private lateinit var getCharactersInteractor: GetCharactersInteractor
    private lateinit var mockCharactersRepository: CharactersRepository

    @Before
    fun setUp() {
        mockCharactersRepository = mock()
        getCharactersInteractor = GetCharactersInteractor(mockCharactersRepository)
    }

    @Test
    fun buildUseCaseObsevableCallsRepository() {
        getCharactersInteractor.buildUseCaseObservable(null)
        verify(mockCharactersRepository).getCharacters(null)
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        stubCharactersRepositoryGetCharacters(Single.just(CharactersFactory.makeCharactersList(2)))
        val testObserver = getCharactersInteractor.buildUseCaseObservable(null).test()
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseObservableReturnsData() {
        val characters = CharactersFactory.makeCharactersList(2)
        stubCharactersRepositoryGetCharacters(Single.just(characters))
        val testObserver = getCharactersInteractor.buildUseCaseObservable(null).test()
        testObserver.assertValue(characters)
    }

    private fun stubCharactersRepositoryGetCharacters(single: Single<List<Character>>) {
        whenever(mockCharactersRepository.getCharacters(null))
                .thenReturn(single)
    }
}