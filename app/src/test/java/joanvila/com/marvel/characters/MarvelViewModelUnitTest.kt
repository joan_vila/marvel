package joanvila.com.marvel.characters

import com.nhaarman.mockito_kotlin.anyOrNull
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.interactor.AddFavoriteCharacterInteractor
import joanvila.com.domain.interactor.DeleteFavoriteCharacterInteractor
import joanvila.com.domain.interactor.GetCharactersInteractor
import joanvila.com.domain.model.Character
import joanvila.com.marvel.factory.CharactersFactory
import joanvila.com.marvel.mapper.CharacterMapper
import joanvila.com.marvel.model.CharacterViewEntity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock

@RunWith(JUnit4::class)
class MarvelViewModelUnitTest {

    @Mock lateinit var mockGetCharactersInteractor: GetCharactersInteractor
    @Mock lateinit var mockDeleteCharacterInteractor: DeleteFavoriteCharacterInteractor
    @Mock lateinit var mockAddFavoriteCharacterInteractor: AddFavoriteCharacterInteractor
    @Mock lateinit var mockCharacterMapper: CharacterMapper
    private lateinit var charactersViewModel: CharactersViewModel
    private lateinit var charactersProcessor: CharactersProcessor

    @Before
    fun setUp() {
        mockGetCharactersInteractor = mock()
        mockDeleteCharacterInteractor = mock()
        mockAddFavoriteCharacterInteractor = mock()
        mockCharacterMapper = mock()
        charactersProcessor = CharactersProcessor(
                mockGetCharactersInteractor,
                mockDeleteCharacterInteractor,
                mockAddFavoriteCharacterInteractor,
                mockCharacterMapper,
                Schedulers.trampoline(),
                Schedulers.trampoline()
        )

        charactersViewModel = CharactersViewModel(charactersProcessor)
    }

    @Test
    fun loadCharactersIntentReturnNewDataUiModel() {
        // Given
        val list = CharactersFactory.makeCharactersList(2)
        stubGetCharacters(Single.just(list))
        // When
        val testObserver = charactersViewModel.states().test()
        charactersViewModel.processIntents(Observable.just(CharactersIntent.LoadIntent()))
        // Then
        testObserver.assertValueAt(2, { it is CharactersUiModel.NewData })
    }

    @Test
    fun loadCharactersIntentReturnsData() {
        // Given
        val list = CharactersFactory.makeCharactersList(2)
        val viewList = CharactersFactory.makeCharacterViewEntityList(2)
        stubCharacterMapperMapToView(viewList[0], list[0])
        stubCharacterMapperMapToView(viewList[1], list[1])
        stubGetCharacters(Single.just(list))
        // When
        val testObserver = charactersViewModel.states().test()
        charactersViewModel.processIntents(Observable.just(CharactersIntent.LoadIntent()))
        // Then
        testObserver.assertValueAt(2, { (it as? CharactersUiModel.NewData)?.data == viewList } )
    }

    @Test
    fun loadCharactersIntentReturnsError() {
        // Given
        stubGetCharacters(Single.error(RuntimeException()))
        // When
        val testObserver = charactersViewModel.states().test()
        charactersViewModel.processIntents(Observable.just(CharactersIntent.LoadIntent()))
        // Then
        testObserver.assertValueAt(2, { it is CharactersUiModel.Failed } )
    }

    @Test
    fun loadCharactersIntentWhenErrorIsNotInProgress() {
        // Given
        stubGetCharacters(Single.error(RuntimeException()))
        // When
        val testObserver = charactersViewModel.states().test()
        charactersViewModel.processIntents(Observable.just(CharactersIntent.LoadIntent()))
        // Then
        testObserver.assertValueAt(2, { !it.inProgress } )
    }

    private fun stubCharacterMapperMapToView(characterViewEntity: CharacterViewEntity,
                                             character: Character) {
        whenever(mockCharacterMapper.mapToViewEntity(character))
                .thenReturn(characterViewEntity)
    }

    private fun stubGetCharacters(single: Single<List<Character>>) {
        whenever(mockGetCharactersInteractor.execute(anyOrNull()))
                .thenReturn(single)
    }
}