package joanvila.com.marvel.factory

import joanvila.com.domain.model.Character
import joanvila.com.marvel.model.CharacterViewEntity
import java.util.concurrent.ThreadLocalRandom

class CharactersFactory {

    companion object Factory {

        fun makeCharactersList(count: Int): List<Character> {
            val characters = mutableListOf<Character>()
            repeat(count) {
                characters.add(makeCharacterModel())
            }
            return characters
        }

        fun makeCharacterModel(): Character {
            return Character(
                    randomLong(),
                    randomText(),
                    randomText(),
                    randomText(),
                    false
            )
        }

        fun randomText(): String {
            return java.util.UUID.randomUUID().toString()
        }

        fun randomLong(): Long {
            return ThreadLocalRandom.current().nextInt(0, 10).toLong()
        }

        fun makeCharacterViewEntityList(count: Int): List<CharacterViewEntity> {
            val characterViews = mutableListOf<CharacterViewEntity>()
            repeat(count) {
                characterViews.add(makeCharacterViewEntity(it.toLong()))
            }
            return characterViews
        }

        fun makeCharacterViewEntity(id: Long): CharacterViewEntity {
            return CharacterViewEntity(id,
                    randomText(),
                    randomText(),
                    randomText(),
                    false)
        }
    }


}