package joanvila.com.marvel.conditionWatcher.instructions

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.marvel.R
import joanvila.com.marvel.conditionWatcher.Instruction

class GetCharactersDataInstruction() : Instruction() {

    override val description: String = "Waiting for the characters list to be populated"

    override fun checkCondition(activity: AppCompatActivity): Boolean {
        val rvCharacters = activity.findViewById(R.id.rv_characters) as RecyclerView
        return rvCharacters.adapter != null && rvCharacters.adapter!!.itemCount > 1
    }
}