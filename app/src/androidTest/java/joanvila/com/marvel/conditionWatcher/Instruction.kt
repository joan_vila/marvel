package joanvila.com.marvel.conditionWatcher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


abstract class Instruction {

    private var dataContainer = Bundle()

    abstract val description: String

    fun setData(dataContainer: Bundle) {
        this.dataContainer = dataContainer
    }

    abstract fun checkCondition(activity: AppCompatActivity): Boolean
}