package joanvila.com.marvel

import android.content.pm.ActivityInfo
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import joanvila.com.marvel.splash.SplashActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SplashTest {

    private val splashScreenWaitingTime = 3000L

    @Rule
    @JvmField  var splashActivityRule: ActivityTestRule<SplashActivity> = ActivityTestRule(SplashActivity::class.java)
    private lateinit var splashActivity: SplashActivity

    @Before
    fun setUp() {
        this.splashActivity = splashActivityRule.activity
    }

    @Test
    fun splashActivityDisplaysImage() {
        onView(withId(R.id.iv_splash)).check(matches(isDisplayed()))
    }

    @Test
    fun splashActivityDisplaysImageWhenRotating() {
        onView(withId(R.id.iv_splash)).check(matches(isDisplayed()))
        splashActivityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        onView(withId(R.id.iv_splash)).check(matches(isDisplayed()))
    }

    @Test
    fun splashActivityWaitsAndJumpsToCharactersActivity() {
        Thread.sleep(splashScreenWaitingTime)
        onView(withId(R.id.rv_characters)).check(matches(isDisplayed()))
    }
}