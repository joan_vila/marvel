package joanvila.com.marvel

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import joanvila.com.marvel.characters.CharactersActivity
import joanvila.com.marvel.conditionWatcher.ConditionWatcher
import joanvila.com.marvel.conditionWatcher.instructions.GetCharactersDataInstruction
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CharactersTest {

    @Rule
    @JvmField  var charactersActivityRule: ActivityTestRule<CharactersActivity> = ActivityTestRule(CharactersActivity::class.java)
    private lateinit var charactersActivity: CharactersActivity

    private val getCharactersDataInstruction =  GetCharactersDataInstruction()

    @Before
    fun setUp() {
        this.charactersActivity = charactersActivityRule.activity
    }

    @Test
    fun charactersActivityDisplaysImage() {
        onView(withId(R.id.rv_characters)).check(matches(isDisplayed()))
    }

    @Test
    fun charactersActivityClickOnCharacterNavigateToDetail() {
        ConditionWatcher.waitForCondition(charactersActivity, getCharactersDataInstruction)
        onView(allOf(withId(R.id.tv_name), withText("A.I.M."))).perform(click())
        onView(withId(R.id.tv_description)).check(matches(isDisplayed()))
    }

}