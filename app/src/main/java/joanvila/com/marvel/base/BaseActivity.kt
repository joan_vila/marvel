package joanvila.com.marvel.base

import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.evernote.android.state.StateSaver
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import joanvila.com.marvel.navigation.Navigator
import joanvila.com.marvel.utils.extractErrorMessage
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject lateinit var navigator: Navigator
    abstract var layout: Int
    abstract fun onViewLoaded()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layout)
        StateSaver.restoreInstanceState(this, savedInstanceState)
        onViewLoaded()
    }

    protected fun showError(exception: Throwable?) {
        val errorMsg = exception?.let {
            it.extractErrorMessage(this)
        } ?: "Something wrong happened"
        showSnackBar(window.decorView.findViewById(android.R.id.content),
                errorMsg)
    }

    private fun showSnackBar(rootLayout: ViewGroup, message: String) {
        Snackbar.make(
                rootLayout, // Parent view
                message, // Message to show
                Snackbar.LENGTH_LONG //
        ).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return false
    }
}