package joanvila.com.marvel.base

import androidx.lifecycle.ViewModel
import joanvila.com.marvel.splash.SplashViewModel
import joanvila.com.marvel.injection.module.DataModule
import joanvila.com.marvel.injection.viewmodel.DaggerViewModelInjector
import joanvila.com.marvel.injection.viewmodel.ViewModelInjector
import joanvila.com.marvel.characters.CharactersViewModel

abstract class BaseViewModel: ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .dataModule(DataModule())
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is SplashViewModel -> injector.inject(this)
            is CharactersViewModel -> injector.inject(this)
        }
    }
}