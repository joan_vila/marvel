package joanvila.com.marvel.base

import joanvila.com.marvel.model.TaskStatus

interface BaseResult {
    val status: TaskStatus
    val error: Throwable?
}