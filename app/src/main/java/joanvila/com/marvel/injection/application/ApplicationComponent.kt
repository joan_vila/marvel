package joanvila.com.marvel.injection.application

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import joanvila.com.marvel.BaseApplication
import joanvila.com.marvel.injection.module.ui.ActivityInjector
import joanvila.com.marvel.injection.module.ApplicationModule
import joanvila.com.marvel.injection.module.CacheModule
import joanvila.com.marvel.injection.module.DataModule
import joanvila.com.marvel.injection.module.RemoteModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, AndroidInjectionModule::class,
    ActivityInjector::class, DataModule::class, RemoteModule::class, CacheModule::class])

interface ApplicationComponent {

    fun inject(application: BaseApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }
}