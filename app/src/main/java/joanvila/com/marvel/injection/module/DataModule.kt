package joanvila.com.marvel.injection.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import joanvila.com.cache.CharactersCacheImpl
import joanvila.com.cache.db.AppDatabase
import joanvila.com.data.CharactersDataRepository
import joanvila.com.data.repository.CharactersCache
import joanvila.com.data.repository.CharactersRemote
import joanvila.com.domain.repository.CharactersRepository
import joanvila.com.marvel.injection.qualifier.ApplicationContext
import joanvila.com.remote.CharactersRemoteImpl
import joanvila.com.remote.CharactersService
import joanvila.com.remote.CharactersServiceFactory
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesCharactersDataRepository(charactersDataRepository: CharactersDataRepository): CharactersRepository {
        return charactersDataRepository
    }

}