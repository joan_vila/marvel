package joanvila.com.marvel.injection.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import joanvila.com.cache.CharactersCacheImpl
import joanvila.com.cache.db.AppDatabase
import joanvila.com.data.repository.CharactersCache
import joanvila.com.marvel.injection.qualifier.ApplicationContext
import javax.inject.Singleton

@Module
class CacheModule {

    @Provides
    fun providesAppDatabase(@ApplicationContext context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "marvel-db").allowMainThreadQueries().build()

    @Provides
    fun providesFavouriteCharacterDao(database: AppDatabase) = database.favouriteCharacterDao()

    @Provides
    @Singleton
    fun providesCharactersCached(charactersCacheImpl: CharactersCacheImpl): CharactersCache {
        return charactersCacheImpl
    }

}