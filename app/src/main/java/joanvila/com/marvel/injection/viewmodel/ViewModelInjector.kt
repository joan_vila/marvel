package joanvila.com.marvel.injection.viewmodel

import dagger.Component
import joanvila.com.marvel.splash.SplashViewModel
import joanvila.com.marvel.injection.module.DataModule
import joanvila.com.marvel.characters.CharactersViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [(DataModule::class)])
interface ViewModelInjector {

    fun inject(splashViewModel: SplashViewModel)
    fun inject(charactersViewModel: CharactersViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun dataModule(dataModule: DataModule): Builder
    }
}