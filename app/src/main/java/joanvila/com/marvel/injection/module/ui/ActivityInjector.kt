package joanvila.com.marvel.injection.module.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import joanvila.com.marvel.character.CharacterDetailActivity
import joanvila.com.marvel.characters.CharactersActivity
import joanvila.com.marvel.injection.scope.PerActivity
import joanvila.com.marvel.splash.SplashActivity

@Module
abstract class ActivityInjector {

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeSplashInjector(): SplashActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(CharactersActivityModule::class)])
    abstract fun contributeCharactersInjector(): CharactersActivity

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeDetailInjector(): CharacterDetailActivity
}