package joanvila.com.marvel.injection.module.ui

import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import joanvila.com.marvel.characters.CharactersActivity
import joanvila.com.marvel.characters.CharactersAdapterDelegate
import joanvila.com.marvel.injection.scope.PerActivity
import joanvila.com.marvel.utils.list.ViewType
import joanvila.com.marvel.utils.list.AdapterDelegate
import joanvila.com.marvel.utils.list.LoadingAdapterDelegate

@Module
class CharactersActivityModule {

    @Provides
    internal fun provideFriendlyConfigView(activity: CharactersActivity): CharactersActivity {
        return activity
    }

    @Provides
    @PerActivity
    @ElementsIntoSet
    internal fun provideAdapterDelegates() : MutableSet<AdapterDelegate<List<ViewType>>> {
        val delegates = LinkedHashSet<AdapterDelegate<List<ViewType>>>()
        delegates.add(CharactersAdapterDelegate())
        delegates.add(LoadingAdapterDelegate())
        return delegates
    }
}