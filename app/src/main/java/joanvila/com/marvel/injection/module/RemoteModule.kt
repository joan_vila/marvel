package joanvila.com.marvel.injection.module

import dagger.Module
import dagger.Provides
import joanvila.com.data.repository.CharactersRemote
import joanvila.com.remote.CharactersRemoteImpl
import joanvila.com.remote.CharactersService
import joanvila.com.remote.CharactersServiceFactory
import joanvila.com.remote.utils.ConnectionChecker
import joanvila.com.remote.utils.ConnectionInterface
import javax.inject.Singleton

@Module
class RemoteModule {

    @Provides
    @Singleton
    fun providesCharactersService(): CharactersService {
        return CharactersServiceFactory.createCharactersService()
    }

    @Provides
    @Singleton
    fun providesCharactersRemote(charactersRemoteImpl: CharactersRemoteImpl): CharactersRemote {
        return charactersRemoteImpl
    }

    @Provides
    @Singleton
    fun providesConnectionChecker(connectionChecker: ConnectionChecker): ConnectionInterface {
        return connectionChecker
    }
}