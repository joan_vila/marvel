package joanvila.com.marvel.characters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.marvel.R
import joanvila.com.marvel.model.CharacterViewEntity
import joanvila.com.marvel.utils.list.ViewType
import joanvila.com.marvel.utils.list.ViewTypeConstants
import joanvila.com.marvel.utils.list.AdapterDelegate
import joanvila.com.marvel.utils.list.BaseListener
import joanvila.com.marvel.utils.list.ItemViewHolder
import joanvila.com.marvel.utils.load
import joanvila.com.marvel.utils.onClick
import kotlinx.android.synthetic.main.item_character.view.*

class CharactersAdapterDelegate: AdapterDelegate<List<ViewType>>() {
    interface CharacterListener: BaseListener {
        fun onItemClicked(character: CharacterViewEntity)
        fun onFavouriteIconClicked(character: CharacterViewEntity)
    }

    override var viewTypeId = ViewTypeConstants.VIEW_TYPE_CHARACTER

    override fun isForViewType(items: List<ViewType>, position: Int) = items[position] is CharacterViewEntity

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_character, parent, false)
        return CharacterViewHolder(view, listener, context)
    }

    override fun onBindViewHolder(items: List<ViewType>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        (holder as? CharacterViewHolder)?.bind(items[position] as CharacterViewEntity)
    }

    class CharacterViewHolder(view: View, val listener: BaseListener?,
                            val context: Context) : ItemViewHolder(view) {
        fun bind(character: CharacterViewEntity) {
            with(itemView) {
                tv_name.text = character.name
                iv_character.load(character.characterThumbnail)
                if (character.isFavourite) {
                    iv_favourite.setImageDrawable(
                            ContextCompat.getDrawable(itemView.context, R.drawable.ic_unfavourite))
                } else {
                    iv_favourite.setImageDrawable(
                            ContextCompat.getDrawable(itemView.context, R.drawable.ic_favourite))
                }
                iv_favourite.onClick {
                    listener?.let { baseListener ->
                        (baseListener as CharacterListener).onFavouriteIconClicked(character)
                    }
                }
                onClick {
                    listener?.let { baseListener ->
                        (baseListener as CharacterListener).onItemClicked(character)
                    }
                }
            }
        }
    }
}