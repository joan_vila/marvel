package joanvila.com.marvel.characters

import joanvila.com.marvel.base.BaseResult
import joanvila.com.marvel.model.CharacterViewEntity
import joanvila.com.marvel.model.TaskStatus

sealed class CharactersResult: BaseResult {

    data class GetCharacters(
            val loadMore: Boolean = false,
            val data: List<CharacterViewEntity>? = null,
            override val status: TaskStatus,
            override val error: Throwable?
    ): CharactersResult()

    data class AddFavouriteCharacter(
            val character: CharacterViewEntity? = null,
            override val status: TaskStatus,
            override val error: Throwable?
    ): CharactersResult()

    data class DeleteFavouriteCharacter(
            val character: CharacterViewEntity? = null,
            override val status: TaskStatus,
            override val error: Throwable?
    ): CharactersResult()
}