package joanvila.com.marvel.characters

import joanvila.com.marvel.model.CharacterViewEntity

sealed class CharactersUiModel(
        val inProgress: Boolean = false,
        val error: Throwable? = null
) {
    object InProgress : CharactersUiModel(true)
    data class Failed(val exception: Throwable?) : CharactersUiModel(false, exception)
    data class NewData(val data: List<CharacterViewEntity>?, val loadMore: Boolean) : CharactersUiModel()
    data class FavouriteChanged(val result: CharacterViewEntity?): CharactersUiModel()
    object Idle : CharactersUiModel(false)
}