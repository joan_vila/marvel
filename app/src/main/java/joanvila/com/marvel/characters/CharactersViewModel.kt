package joanvila.com.marvel.characters

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import joanvila.com.marvel.base.BaseViewModel
import joanvila.com.marvel.model.TaskStatus
import javax.inject.Inject

class CharactersViewModel @Inject constructor(private val charactersProcessor: CharactersProcessor)
    : BaseViewModel() {

    private val intentsSubject: PublishSubject<CharactersIntent> = PublishSubject.create()
    private val statesObservable: Observable<CharactersUiModel> = compose()

    fun processIntents(intents: Observable<CharactersIntent>) {
        intents.subscribe(intentsSubject)
    }

    fun states() = statesObservable.onErrorResumeNext { error: Throwable ->
        Observable.just(CharactersUiModel.Failed(error))
    }

    private val intentFilter: ObservableTransformer<CharactersIntent, CharactersIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge<CharactersIntent>(
                        shared.ofType(CharactersIntent.InitialIntent::class.java).take(1),
                        shared.filter { it !is CharactersIntent.InitialIntent }
                )
            }
        }

    private fun compose() =
            intentsSubject
                    .compose(intentFilter)
                    .map { this.actionFromIntent(it) }
                    .compose(charactersProcessor.actionProcessor)
                    .scan(CharactersUiModel.Idle, reducer)
                    .distinctUntilChanged()
                    .replay(1)
                    .autoConnect(0)

    private fun actionFromIntent(intent: CharactersIntent): CharactersAction {
        return when (intent) {
            is CharactersIntent.InitialIntent -> CharactersAction.LoadCharacters(0)
            is CharactersIntent.LoadIntent -> CharactersAction.LoadCharacters(intent.offset ?: 0)
            is CharactersIntent.OnFavouriteClickedIntent -> {
                if (intent.character.isFavourite) {
                    CharactersAction.DeleteFavouriteCharacter(intent.character)
                } else {
                    CharactersAction.AddFavouriteCharacter(intent.character)
                }
            }
        }
    }

    private val reducer
            get() = BiFunction { _: CharactersUiModel, result: CharactersResult ->
                when {
                    result.status == TaskStatus.FAILURE -> CharactersUiModel.Failed(result.error)
                    result.status == TaskStatus.IN_FLIGHT -> CharactersUiModel.InProgress
                    else -> {
                        when (result) {
                            is CharactersResult.GetCharacters -> CharactersUiModel.NewData(result.data, result.loadMore)
                            is CharactersResult.AddFavouriteCharacter -> {
                                val character= result.character?.copy(isFavourite = true)
                                CharactersUiModel.FavouriteChanged(character)
                            }
                            is CharactersResult.DeleteFavouriteCharacter -> {
                                val character = result.character?.copy(isFavourite = false)
                                CharactersUiModel.FavouriteChanged(character)
                            }
                        }
                    }
                }
            }
}