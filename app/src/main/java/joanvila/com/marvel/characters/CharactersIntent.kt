package joanvila.com.marvel.characters

import joanvila.com.marvel.model.CharacterViewEntity

sealed class CharactersIntent {
    object InitialIntent: CharactersIntent()
    data class LoadIntent(val offset: Int? = 0): CharactersIntent()
    data class OnFavouriteClickedIntent(val character: CharacterViewEntity): CharactersIntent()
}