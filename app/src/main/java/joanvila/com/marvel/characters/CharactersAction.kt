package joanvila.com.marvel.characters

import joanvila.com.marvel.model.CharacterViewEntity

sealed class CharactersAction {
    data class LoadCharacters(val offset: Int): CharactersAction()
    data class AddFavouriteCharacter(val character: CharacterViewEntity): CharactersAction()
    data class DeleteFavouriteCharacter(val character: CharacterViewEntity): CharactersAction()
}

