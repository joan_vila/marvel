package joanvila.com.marvel.characters

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.GridLayoutManager
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import joanvila.com.marvel.R
import joanvila.com.marvel.base.BaseActivity
import joanvila.com.marvel.model.CharacterViewEntity
import joanvila.com.marvel.utils.list.EndlessRecyclerOnScrollListener
import joanvila.com.marvel.utils.list.GenericAdapter
import joanvila.com.marvel.utils.setGone
import joanvila.com.marvel.utils.setVisible
import joanvila.com.marvel.utils.setupWithEndless
import kotlinx.android.synthetic.main.activity_characters.*
import javax.inject.Inject

class CharactersActivity: BaseActivity(), CharactersAdapterDelegate.CharacterListener {

    private var disposables = CompositeDisposable()
    override var layout = R.layout.activity_characters
    private lateinit var scrollListener: EndlessRecyclerOnScrollListener
    @Inject lateinit var adapter: GenericAdapter
    @Inject lateinit var viewModel: CharactersViewModel
    private val loadIntent: PublishSubject<CharactersIntent.LoadIntent> = PublishSubject.create()
    private val onFavouriteClickedIntent: PublishSubject<CharactersIntent.OnFavouriteClickedIntent> = PublishSubject.create()

    companion object {
        @JvmStatic fun getIntent(context: Context) = Intent(context, CharactersActivity::class.java)
    }

    override fun onViewLoaded() {
        configViews()
        disposables.add(viewModel.states().subscribe(this::render))
        viewModel.processIntents(intents())
    }

    private fun configViews() {
        // Config recycler view
        adapter.setClickListener(this)
        scrollListener = rv_characters.setupWithEndless(
                adapter,
                GridLayoutManager(this, 2),
                null,
                loadMore = { _, total ->
                    loadIntent.onNext(CharactersIntent.LoadIntent(total))
                })
    }

    private fun intents(): Observable<CharactersIntent> {
        return Observable.merge(listOf(initialIntent(), retryIntent(), loadIntent, onFavouriteClickedIntent))
    }

    private fun initialIntent(): Observable<CharactersIntent.InitialIntent> {
        return Observable.just(CharactersIntent.InitialIntent)
    }

    private fun retryIntent(): Observable<CharactersIntent.LoadIntent> {
        return tv_empty.clicks().map { CharactersIntent.LoadIntent(0) }
    }

    private fun render(state: CharactersUiModel) {
        when(state) {
            is CharactersUiModel.NewData -> {
                state.data?.let {
                    if (it.isEmpty()) {
                        setEmptyVisibility(true)
                        return
                    }
                    if (state.loadMore) {
                        loadMoreCharacters(it)
                    } else {
                        resetCharacters(it)
                    }
                } ?: setEmptyVisibility(true)
            }
            is CharactersUiModel.InProgress -> {
                setEmptyVisibility(false)
                adapter.addLoadingView()
            }
            is CharactersUiModel.FavouriteChanged -> {
                state.result?.let { character ->
                    setEmptyVisibility(false)
                    adapter.removeLoadingView()
                    updateCharacter(character)
                }
            }
            is CharactersUiModel.Failed -> {
                adapter.removeLoadingView()
                setEmptyVisibility(true)
                showError(state.exception)
            }
        }
    }

    private fun setEmptyVisibility(isVisible: Boolean) {
        if (isVisible) {
            tv_empty.setVisible()
            rv_characters.setGone()
        } else {
            tv_empty.setGone()
            rv_characters.setVisible()
        }
    }
    private fun updateCharacter(character: CharacterViewEntity) {
        val index = (adapter.getData() as? ArrayList<CharacterViewEntity>)?.indexOfFirst { it.id == character.id }
        if (index != null && index >= 0) {
            (adapter.getData() as ArrayList)[index] = character
            adapter.notifyItemChanged(index)
        }
    }

    private fun loadMoreCharacters(characters: List<CharacterViewEntity>) {
        adapter.addAll(characters)
    }

    private fun resetCharacters(characters: List<CharacterViewEntity>) {
        scrollListener.resetState()
        adapter.set(newItems = characters)
    }

    override fun onItemClicked(character: CharacterViewEntity) {
        navigator.navigateToDetailActivity(this, character)
    }

    override fun onFavouriteIconClicked(character: CharacterViewEntity) {
        onFavouriteClickedIntent.onNext(CharactersIntent.OnFavouriteClickedIntent(character))
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

}