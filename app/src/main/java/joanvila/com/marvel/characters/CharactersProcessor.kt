package joanvila.com.marvel.characters

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.Single
import joanvila.com.domain.interactor.AddFavoriteCharacterInteractor
import joanvila.com.domain.interactor.DeleteFavoriteCharacterInteractor
import joanvila.com.domain.interactor.GetCharactersInteractor
import joanvila.com.marvel.injection.module.SCHEDULERS_IO
import joanvila.com.marvel.injection.module.SCHEDULERS_MAIN_THREAD
import joanvila.com.marvel.mapper.CharacterMapper
import joanvila.com.marvel.model.TaskStatus
import javax.inject.Inject
import javax.inject.Named

class CharactersProcessor @Inject constructor(
        private val getCharactersInteractor: GetCharactersInteractor,
        private val deleteCharacterInteractor: DeleteFavoriteCharacterInteractor,
        private val addFavoriteCharacterInteractor: AddFavoriteCharacterInteractor,
        private val mapper: CharacterMapper,
        @Named(SCHEDULERS_IO) val subscribeOnScheduler: Scheduler,
        @Named(SCHEDULERS_MAIN_THREAD) val observeOnScheduler: Scheduler) {

    private val loadCharacters: ObservableTransformer<CharactersAction.LoadCharacters, CharactersResult>
        get() = ObservableTransformer { actions ->
            actions.switchMap { action ->
                getCharactersInteractor.execute(action.offset)
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .map { characters ->
                            CharactersResult.GetCharacters(
                                    action.offset > 0,
                                    characters.map { mapper.mapToViewEntity(it) }, TaskStatus.SUCCESS,
                                    null)
                        }
                        .onErrorResumeNext { error ->
                            Single.just(CharactersResult.GetCharacters(false, null,
                                    TaskStatus.FAILURE, error)
                            )
                        }
                        .toObservable()
                        .startWith(CharactersResult.GetCharacters(false, null,
                                TaskStatus.IN_FLIGHT, null)
                        )
            }
        }

    private val addFavouriteCharacter: ObservableTransformer<CharactersAction.AddFavouriteCharacter, CharactersResult>
        get() = ObservableTransformer { actions ->
            actions.switchMap { action ->
                addFavoriteCharacterInteractor.execute(mapper.mapFromViewEntity(action.character))
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .andThen(
                                Single.just(CharactersResult.AddFavouriteCharacter(action.character, TaskStatus.SUCCESS, null)))
                        .onErrorResumeNext {
                            Single.just(CharactersResult.AddFavouriteCharacter(null, TaskStatus.FAILURE, it))
                        }
                        .toObservable()
                        .startWith(CharactersResult.AddFavouriteCharacter(null, TaskStatus.IN_FLIGHT, null))
            }
        }

    private val deleteFavouriteCharacter: ObservableTransformer<CharactersAction.DeleteFavouriteCharacter, CharactersResult>
        get() = ObservableTransformer { actions ->
            actions.switchMap { action ->
                deleteCharacterInteractor.execute(action.character.id)
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .andThen(
                                Single.just(CharactersResult.DeleteFavouriteCharacter(action.character, TaskStatus.SUCCESS, null))
                        )
                        .onErrorResumeNext {
                            Single.just(CharactersResult.DeleteFavouriteCharacter(null, TaskStatus.FAILURE, it))
                        }
                        .toObservable()
                        .startWith(CharactersResult.DeleteFavouriteCharacter(null, TaskStatus.IN_FLIGHT, null))
            }
        }

    internal val actionProcessor: ObservableTransformer<CharactersAction, CharactersResult>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge(arrayListOf(
                        shared.ofType(CharactersAction.LoadCharacters::class.java).compose(loadCharacters),
                        shared.ofType(CharactersAction.AddFavouriteCharacter::class.java).compose(addFavouriteCharacter),
                        shared.ofType(CharactersAction.DeleteFavouriteCharacter::class.java).compose(deleteFavouriteCharacter)
                ))
            }
        }
}