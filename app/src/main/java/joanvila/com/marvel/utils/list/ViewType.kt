package joanvila.com.marvel.utils.list

import android.os.Parcelable

interface ViewType: Parcelable {
    fun getViewType(): Int
}