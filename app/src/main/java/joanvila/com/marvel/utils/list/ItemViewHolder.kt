package joanvila.com.marvel.utils.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)