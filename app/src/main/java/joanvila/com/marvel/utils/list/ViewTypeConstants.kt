package joanvila.com.marvel.utils.list

object ViewTypeConstants {
    const val VIEW_TYPE_CHARACTER = 1
    const val VIEW_TYPE_LOADING = 99
}
