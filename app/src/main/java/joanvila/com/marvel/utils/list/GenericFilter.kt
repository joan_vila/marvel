package joanvila.com.marvel.utils.list

import android.widget.Filter

class GenericFilter(private val genericAdapter: GenericAdapter,
                    private var allItems: List<ViewType>,
                    private val filterToExecute: (it: ViewType, query: String) -> Boolean) : Filter() {

    private val filteredItems: ArrayList<ViewType> = arrayListOf()

    fun updateAllItems(allItems: List<ViewType>) {
        this.allItems = allItems
    }

    override fun performFiltering(constraint: CharSequence?) : FilterResults {
        filteredItems.clear()
        val results = FilterResults()

        if (constraint.isNullOrBlank()) {
            filteredItems.addAll(allItems)
        } else {
            val query = constraint.toString().toLowerCase().trim()
            allItems.filterTo(filteredItems) { filterToExecute(it, query) }
        }
        results.values = filteredItems
        results.count = filteredItems.size
        return results
    }

    override fun publishResults(p0: CharSequence?, results: FilterResults?) {
        if (results != null) {
            genericAdapter.publishFilteredResults(results.values as List<ViewType>)
        }
    }
}