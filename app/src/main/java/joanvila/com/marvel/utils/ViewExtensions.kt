package joanvila.com.marvel.utils

import android.os.SystemClock
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import joanvila.com.marvel.R
import joanvila.com.marvel.utils.list.EndlessRecyclerOnScrollListener
import joanvila.com.marvel.utils.list.GenericAdapter
import java.util.*

fun View.setVisible() {
    this.visibility = View.VISIBLE
}

fun View.setInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.setGone() {
    this.visibility = View.GONE
}

fun View.onClick(interval: Long = 1000, action: (View) -> Unit) {
    val lastClickMap = WeakHashMap<View, Long>()

    setOnClickListener {
        val previousClickTimestamp = lastClickMap[it]
        val currentTimestamp = SystemClock.uptimeMillis()
        lastClickMap[it] = currentTimestamp

        if( previousClickTimestamp == null ||
            Math.abs(currentTimestamp - previousClickTimestamp) > interval) {
                action(this)
        }
    }
}

fun RecyclerView.setupWithEndless(adapter: GenericAdapter,
                                  layoutManager: RecyclerView.LayoutManager,
                                  itemDecoration: RecyclerView.ItemDecoration? = null,
                                  loadMore: (Int, Int) -> Unit): EndlessRecyclerOnScrollListener {

    this.layoutManager = layoutManager
    this.adapter = adapter

    itemDecoration?.let { this.addItemDecoration(it) }
    val customScrollListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
        override fun onLoadMore(page: Int, totalItemsCount: Int) {
            loadMore(page, totalItemsCount)
        }
    }
    this.clearOnScrollListeners()
    this.addOnScrollListener(customScrollListener)
    return customScrollListener
}

fun ImageView.load(url: String, placeHolder: Int = R.color.colorPrimary) {
    Picasso.get().load(url).placeholder(placeHolder).fit().centerCrop().into(this)
}


