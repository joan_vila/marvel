package joanvila.com.marvel.utils.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.marvel.R

/**
 * This [AdapterDelegate] will be needed for the [GenericAdapter] for providing loaders
 * when dealing with infinite lists
 */
class LoadingAdapterDelegate : AdapterDelegate<List<ViewType>>() {

    override var viewTypeId = ViewTypeConstants.VIEW_TYPE_LOADING

    override fun onBindViewHolder(items: List<ViewType>, position: Int,
                                  holder: RecyclerView.ViewHolder, payloads: List<Any>) {}

    override fun isForViewType(items: List<ViewType>, position: Int) =
            items[position] is LoadingViewType

    override fun onCreateViewHolder(parent: ViewGroup): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_infinite_loader,
                                                               parent, false)
        return LoadingViewHolder(view!!)
    }

    class LoadingViewHolder(view: View) : ItemViewHolder(view)
}