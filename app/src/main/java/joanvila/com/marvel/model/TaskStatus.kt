package joanvila.com.marvel.model

enum class TaskStatus {
    SUCCESS, FAILURE, IN_FLIGHT
}