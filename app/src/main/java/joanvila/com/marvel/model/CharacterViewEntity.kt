package joanvila.com.marvel.model

import joanvila.com.marvel.utils.list.ViewType
import joanvila.com.marvel.utils.list.ViewTypeConstants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CharacterViewEntity(
        val id: Long,
        val name: String,
        val description: String,
        val characterThumbnail: String,
        val isFavourite: Boolean
) : ViewType {
    override fun getViewType() = ViewTypeConstants.VIEW_TYPE_CHARACTER
}