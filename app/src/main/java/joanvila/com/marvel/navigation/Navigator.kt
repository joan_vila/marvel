package joanvila.com.marvel.navigation

import android.content.Context
import joanvila.com.marvel.character.CharacterDetailActivity
import joanvila.com.marvel.characters.CharactersActivity
import joanvila.com.marvel.model.CharacterViewEntity
import javax.inject.Inject

class Navigator @Inject constructor() {

    fun navigateToMainActivity(context: Context) {
        context.startActivity(CharactersActivity.getIntent(context))
    }

    fun navigateToDetailActivity(context: Context, character: CharacterViewEntity) {
        context.startActivity(CharacterDetailActivity.getIntent(context, character))
    }
}
