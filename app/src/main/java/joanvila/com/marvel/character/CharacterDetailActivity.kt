package joanvila.com.marvel.character

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import joanvila.com.marvel.R
import joanvila.com.marvel.base.BaseActivity
import joanvila.com.marvel.model.CharacterViewEntity
import joanvila.com.marvel.utils.load
import kotlinx.android.synthetic.main.activity_character_detail.*

class CharacterDetailActivity: BaseActivity() {

    override var layout = R.layout.activity_character_detail
    private lateinit var character: CharacterViewEntity

    companion object {

        const val CHARACTER_EXTRA = "CHARACTER"

        @JvmStatic fun getIntent(context: Context, character: CharacterViewEntity): Intent {
            val intent = Intent(context, CharacterDetailActivity::class.java)
            intent.putExtra(CHARACTER_EXTRA, character)
            return intent
        }
    }

    override fun onViewLoaded() {
        intent.getParcelableExtra<CharacterViewEntity>(CHARACTER_EXTRA)?.let {
            character = it
        } ?: finish()
        configViews()
    }

    private fun configViews() {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        with(character) {
            iv_character.load(characterThumbnail)
            tv_name.text = name
            tv_description.text = description
            setFavouriteIcon(isFavourite)
        }
    }

    private fun setFavouriteIcon(isFavourite: Boolean) {
        val favouriteIcon = if (isFavourite) {
            R.drawable.ic_unfavourite
        } else {
            R.drawable.ic_favourite
        }
        iv_favourite.setImageDrawable(
                ContextCompat.getDrawable(this@CharacterDetailActivity, favouriteIcon))
    }

}