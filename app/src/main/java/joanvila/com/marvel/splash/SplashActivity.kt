package joanvila.com.marvel.splash

import io.reactivex.disposables.CompositeDisposable
import joanvila.com.marvel.R
import joanvila.com.marvel.base.BaseActivity
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    private var disposables = CompositeDisposable()
    @Inject lateinit var viewModel: SplashViewModel
    override var layout = R.layout.activity_splash

    override fun onViewLoaded() {
        configViews()
        disposables.add(viewModel.wait()
                .subscribe(this::navigateToMainActivity))
    }

    private fun configViews() {
        supportActionBar?.hide()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun navigateToMainActivity() {
        navigator.navigateToMainActivity(this)
        finish()
    }

}
