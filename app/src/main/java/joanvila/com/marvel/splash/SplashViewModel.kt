package joanvila.com.marvel.splash

import io.reactivex.Completable
import io.reactivex.Scheduler
import joanvila.com.marvel.base.BaseViewModel
import joanvila.com.marvel.injection.module.SCHEDULERS_IO
import joanvila.com.marvel.injection.module.SCHEDULERS_MAIN_THREAD
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class SplashViewModel @Inject constructor(
        @Named(SCHEDULERS_IO) val subscribeOnScheduler: Scheduler,
        @Named(SCHEDULERS_MAIN_THREAD) val observeOnScheduler: Scheduler) : BaseViewModel() {

    companion object {
        const val SPLASH_TIME = 3000L
    }

    fun wait() = makeSplashTaskCompletable()
            .observeOn(observeOnScheduler)
            .subscribeOn(subscribeOnScheduler)

    private fun makeSplashTaskCompletable(): Completable {
        return Completable.timer(SPLASH_TIME, TimeUnit.MILLISECONDS)
    }
}