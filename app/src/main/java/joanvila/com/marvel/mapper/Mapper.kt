package joanvila.com.marvel.mapper

interface Mapper<V, D> {
    fun mapToViewEntity(type: D): V
    fun mapFromViewEntity(type: V): D
}