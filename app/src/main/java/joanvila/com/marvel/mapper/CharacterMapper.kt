package joanvila.com.marvel.mapper

import joanvila.com.domain.model.Character
import joanvila.com.marvel.model.CharacterViewEntity
import javax.inject.Inject

open class CharacterMapper @Inject constructor(): Mapper<CharacterViewEntity, Character> {

    override fun mapFromViewEntity(type: CharacterViewEntity): Character {
        return Character(
                type.id,
                type.name,
                type.description,
                type.characterThumbnail,
                type.isFavourite
        )
    }

    override fun mapToViewEntity(type: Character): CharacterViewEntity {
        return CharacterViewEntity(
                type.id,
                type.name,
                type.description,
                type.characterThumbnail,
                type.isFavourite
        )
    }

}