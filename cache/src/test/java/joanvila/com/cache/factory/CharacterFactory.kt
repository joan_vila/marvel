package joanvila.com.cache.factory

import joanvila.com.cache.model.CharacterCachedEntity
import joanvila.com.data.model.CharacterEntity
import java.util.concurrent.ThreadLocalRandom

class CharacterFactory {

    companion object Factory {

        fun createCachedCharacter(): CharacterCachedEntity {
            return CharacterCachedEntity(randomLong(), randomText(), randomText(), randomText())
        }

        fun createCharacterEntity(): CharacterEntity {
            return CharacterEntity(randomLong(), randomText(), randomText(), randomText(), randomBoolean())
        }

        private fun randomText(): String {
            return java.util.UUID.randomUUID().toString()
        }

        private fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
        }

        private fun randomLong(): Long {
            return randomInt().toLong()
        }

        private fun randomBoolean(): Boolean {
            return Math.random() < 0.5
        }
    }
}