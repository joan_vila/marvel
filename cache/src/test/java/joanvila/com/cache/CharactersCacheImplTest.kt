package joanvila.com.cache

import androidx.room.Room
import joanvila.com.cache.db.AppDatabase
import joanvila.com.cache.factory.CharacterFactory
import joanvila.com.cache.mapper.CharacterCachedMapper
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class CharactersCacheImplTest {

    private var appDatabase = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application,
            AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()

    private var mapper = CharacterCachedMapper()

    private val charactersCacheImpl = CharactersCacheImpl(appDatabase, mapper)

    @Test
    fun addFavouriteCharacterCompletes() {
        // Given
        val character = CharacterFactory.createCharacterEntity()
        // When
        val testObserver = charactersCacheImpl.addFavouriteCharacter(character).test()
        // Then
        testObserver.assertComplete()
    }

    @Test
    fun addFavouriteCharacterSavesData() {
        // Given
        val characterEntity1 = CharacterFactory.createCharacterEntity()
        val characterEntity2 = CharacterFactory.createCharacterEntity()
        // When
        charactersCacheImpl.addFavouriteCharacter(characterEntity1).test()
        charactersCacheImpl.addFavouriteCharacter(characterEntity2).test()
        // Then
        assertEquals(appDatabase.favouriteCharacterDao().getAllFavourites().size, 2)
    }

    @Test
    fun deleteFavouriteCharacterCompletes() {
        // Given
        val character = CharacterFactory.createCharacterEntity()
        charactersCacheImpl.addFavouriteCharacter(character).test()
        // When
        val testObserver = charactersCacheImpl.deleteFavouriteCharacter(character.id).test()
        // Then
        testObserver.assertComplete()
    }

}