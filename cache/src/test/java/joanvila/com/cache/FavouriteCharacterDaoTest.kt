package joanvila.com.cache

import android.app.Application
import android.app.Instrumentation
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.reactivex.Single
import joanvila.com.cache.db.AppDatabase
import joanvila.com.cache.factory.CharacterFactory
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
open class FavouriteCharacterDaoTest {

    private lateinit var appDatabase: AppDatabase

    @Before
    fun initDb() {
        appDatabase = Room.inMemoryDatabaseBuilder(
                RuntimeEnvironment.application.baseContext,
                AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @After
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    fun addFavouriteCharacterSavesData() {
        // Given
        val cachedCharacter = CharacterFactory.createCachedCharacter()
        // When
        appDatabase.favouriteCharacterDao().addFavourite(cachedCharacter)
        // Then
        val characters = appDatabase.favouriteCharacterDao().getAllFavourites()
        assert(characters.isNotEmpty())
    }

    @Test
    fun deleteFavouriteCharacterDeletesCharacter() {
        // Given
        val cachedCharacter = CharacterFactory.createCachedCharacter()
        appDatabase.favouriteCharacterDao().addFavourite(cachedCharacter)
        // When
        appDatabase.favouriteCharacterDao().deleteFavourite(cachedCharacter.id)
        // Then
        val characters = appDatabase.favouriteCharacterDao().getAllFavourites()
        assert(characters.isEmpty())
    }

}