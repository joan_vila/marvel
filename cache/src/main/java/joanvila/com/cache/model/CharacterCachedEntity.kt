package joanvila.com.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favourite_characters")
data class CharacterCachedEntity(
        @PrimaryKey
        val id: Long = 0L,
        val name: String = "",
        val description: String = "",
        val thumbnail: String = ""
) {
        constructor() : this(0, "","", "")
}