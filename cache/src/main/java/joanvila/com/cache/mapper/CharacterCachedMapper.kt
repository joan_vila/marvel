package joanvila.com.cache.mapper

import joanvila.com.cache.model.CharacterCachedEntity
import joanvila.com.data.model.CharacterEntity
import javax.inject.Inject

open class CharacterCachedMapper @Inject constructor(): CachedMapper<CharacterCachedEntity, CharacterEntity> {

    override fun mapFromCached(type: CharacterCachedEntity): CharacterEntity {
        return CharacterEntity(
                type.id,
                type.name,
                type.description,
                type.thumbnail,
                true
        )
    }

    override fun mapToCached(type: CharacterEntity): CharacterCachedEntity {
        return CharacterCachedEntity(
                type.id,
                type.name,
                type.description,
                type.characterThumbnail
        )
    }

}