package joanvila.com.cache.mapper

interface CachedMapper<E, D> {
    fun mapFromCached(type: E): D
    fun mapToCached(type: D): E
}