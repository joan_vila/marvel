package joanvila.com.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import joanvila.com.cache.dao.FavouriteCharacterDao
import joanvila.com.cache.model.CharacterCachedEntity

@Database(entities = [CharacterCachedEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favouriteCharacterDao(): FavouriteCharacterDao
}