package joanvila.com.cache

import io.reactivex.Completable
import io.reactivex.Single
import joanvila.com.cache.db.AppDatabase
import joanvila.com.cache.mapper.CharacterCachedMapper
import joanvila.com.data.model.CharacterEntity
import joanvila.com.data.repository.CharactersCache
import javax.inject.Inject

class CharactersCacheImpl @Inject constructor(
        val appDatabase: AppDatabase,
        private val mapper: CharacterCachedMapper): CharactersCache {

    internal fun getDatabase(): AppDatabase {
        return appDatabase
    }

    override fun getFavouriteCharacters(): Single<List<CharacterEntity>> {
        return Single.fromCallable {
            appDatabase.favouriteCharacterDao().getAllFavourites().map { mapper.mapFromCached(it) }
        }
    }

    override fun addFavouriteCharacter(character: CharacterEntity): Completable {
        return Completable.fromCallable {
            appDatabase.favouriteCharacterDao().addFavourite(mapper.mapToCached(character))
        }
    }

    override fun deleteFavouriteCharacter(characterId: Long): Completable {
        return Completable.fromCallable {
            appDatabase.favouriteCharacterDao().deleteFavourite(characterId)
        }
    }

}