package joanvila.com.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import joanvila.com.cache.model.CharacterCachedEntity

@Dao
interface FavouriteCharacterDao {

    @Query("select * from favourite_characters")
    fun getAllFavourites(): List<CharacterCachedEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavourite(characterCachedEntity: CharacterCachedEntity)

    @Query("Delete FROM favourite_characters where id LIKE  :characterId")
    fun deleteFavourite(characterId: Long)

}