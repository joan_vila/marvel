package joanvila.com.data

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import joanvila.com.data.factory.CharacterFactory
import joanvila.com.data.mapper.CharacterMapper
import joanvila.com.data.repository.CharactersCache
import joanvila.com.data.repository.CharactersRemote
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CharactersDataRepositoryTest {

    private lateinit var charactersDataRepository: CharactersDataRepository
    private lateinit var mapper: CharacterMapper
    private lateinit var charactersCacheDataSource: CharactersCache
    private lateinit var charactersRemoteDataSource: CharactersRemote

    @Before
    fun setUp() {
        mapper = CharacterMapper()
        charactersCacheDataSource = mock()
        charactersRemoteDataSource = mock()

        charactersDataRepository = CharactersDataRepository(
                mapper,
                charactersRemoteDataSource,
                charactersCacheDataSource
        )
    }

    @Test
    fun addFavouriteCharacterCompletes() {
        // Given
        stubCharactersCacheAddFavouriteCharacter(Completable.complete())
        val character = CharacterFactory.createCharacter()
        // When
        val testObserver = charactersDataRepository.addFavouriteCharacter(character).test()
        // Then
        testObserver.assertComplete()
    }

    // TODO: Add test for merge cached favourites

    private fun stubCharactersCacheAddFavouriteCharacter(completable: Completable) {
        whenever(charactersCacheDataSource.addFavouriteCharacter(any()))
                .thenReturn(completable)
    }
}