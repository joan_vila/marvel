package joanvila.com.data.factory

import joanvila.com.data.model.CharacterEntity
import joanvila.com.domain.model.Character
import java.util.concurrent.ThreadLocalRandom

class CharacterFactory {
    companion object Factory {

        fun createCharacterEntity(): CharacterEntity {
            return CharacterEntity(randomLong(), randomText(), randomText(), randomText(), randomBoolean())
        }

        fun createCharacter(): Character {
            return Character(randomLong(), randomText(), randomText(), randomText(), randomBoolean())
        }

        fun randomText(): String {
            return java.util.UUID.randomUUID().toString()
        }

        fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
        }

        fun randomLong(): Long {
            return randomInt().toLong()
        }

        fun randomBoolean(): Boolean {
            return Math.random() < 0.5
        }
    }
}