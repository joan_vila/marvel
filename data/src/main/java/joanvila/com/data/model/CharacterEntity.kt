package joanvila.com.data.model

data class CharacterEntity(
        val id: Long,
        val name: String,
        val description: String,
        val characterThumbnail: String,
        val isFavourite: Boolean
)