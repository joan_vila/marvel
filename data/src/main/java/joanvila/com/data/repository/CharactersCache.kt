package joanvila.com.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import joanvila.com.data.model.CharacterEntity

interface CharactersCache {
    fun getFavouriteCharacters(): Single<List<CharacterEntity>>
    fun addFavouriteCharacter(character: CharacterEntity): Completable
    fun deleteFavouriteCharacter(characterId: Long): Completable
}