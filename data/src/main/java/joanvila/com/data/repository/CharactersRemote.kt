package joanvila.com.data.repository

import io.reactivex.Single
import joanvila.com.data.model.CharacterEntity

interface CharactersRemote {
    fun getCharacters(offset: Int): Single<List<CharacterEntity>>
}