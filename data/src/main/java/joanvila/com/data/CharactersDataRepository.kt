package joanvila.com.data

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import joanvila.com.data.mapper.CharacterMapper
import joanvila.com.data.model.CharacterEntity
import joanvila.com.data.repository.CharactersCache
import joanvila.com.data.repository.CharactersRemote
import joanvila.com.data.model.NetworkException
import joanvila.com.domain.model.Character
import joanvila.com.domain.repository.CharactersRepository
import javax.inject.Inject

class CharactersDataRepository @Inject constructor(
        private val mapper: CharacterMapper,
        private val remoteDataSource: CharactersRemote,
        private val cachedDataSource: CharactersCache) : CharactersRepository {

    override fun getCharacters(offset: Int?): Single<List<Character>> {
        val remoteObservable = remoteDataSource.getCharacters(offset ?: 0)
        val cachedObservable = cachedDataSource.getFavouriteCharacters()
        val observable: Single<List<Character>> = Single.zip(remoteObservable, cachedObservable,
                BiFunction { remotes, cached ->
                    val mergedCharacters = remotes as ArrayList<CharacterEntity>
                    val remoteCharactersIds = remotes.map { it.id }
                    val cachedCharactersIds = cached.map { it.id }
                    remoteCharactersIds.forEach { remoteId ->
                        if (cachedCharactersIds.contains(remoteId)) {
                            val index = remotes.indexOfFirst { it.id == remoteId }
                            if (index != -1) {
                                mergedCharacters[index] = remotes[index].copy(isFavourite = true)
                            }
                        }
                    }
                    mergedCharacters.map { mapper.mapFromEntity(it) }
                })
        return observable.onErrorResumeNext { error ->
            if (error is NetworkException.NoInternetConnection && (offset == null || offset == 0)) {
                cachedObservable.map { characters ->
                    characters.map { mapper.mapFromEntity(it) }
                }
            } else {
                Single.error(error)
            }
        }
    }

    override fun addFavouriteCharacter(character: Character): Completable {
        return cachedDataSource.addFavouriteCharacter(character = mapper.mapToEntity(character))
    }

    override fun deleteFavoriteCharacter(id: Long): Completable {
        return cachedDataSource.deleteFavouriteCharacter(id)
    }

}