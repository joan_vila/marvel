package joanvila.com.data.mapper

import joanvila.com.data.model.CharacterEntity
import joanvila.com.domain.model.Character
import javax.inject.Inject

open class CharacterMapper @Inject constructor(): Mapper<CharacterEntity, Character> {

    override fun mapFromEntity(type: CharacterEntity): Character {
        return Character(
                type.id,
                type.name,
                type.description,
                type.characterThumbnail,
                type.isFavourite
        )
    }

    override fun mapToEntity(type: Character): CharacterEntity {
        return CharacterEntity(
                type.id,
                type.name,
                type.description,
                type.characterThumbnail,
                type.isFavourite
        )
    }


}