package joanvila.com.remote.factory

import joanvila.com.remote.model.BaseResponse
import joanvila.com.remote.model.CharacterRemoteEntity
import joanvila.com.remote.model.CharacterResponse
import joanvila.com.remote.model.CharacterThumbnail
import java.util.concurrent.ThreadLocalRandom

class CharacterFactory {

    companion object Factory {

        fun createCharactersResponse(count: Int): BaseResponse<CharacterResponse> {
            val characterResponse = CharacterResponse(count, count, createCharactersModelList(count))
            val characterBaseResponse = BaseResponse<CharacterResponse>(200,null, characterResponse)
            return characterBaseResponse
        }

        private fun createCharactersModelList(count: Int): List<CharacterRemoteEntity> {
            val characters = mutableListOf<CharacterRemoteEntity>()
            repeat(count) {
                characters.add(makeCharacterModel())
            }
            return characters
        }

        private fun makeCharacterModel(): CharacterRemoteEntity {
            return CharacterRemoteEntity(randomLong(), randomText(), randomText(), makeCharacterThumbnailModel())
        }

        private fun makeCharacterThumbnailModel(): CharacterThumbnail {
            return CharacterThumbnail(randomText(), randomText())
        }

        private fun randomText(): String {
            return java.util.UUID.randomUUID().toString()
        }

        private fun randomLong(): Long {
            return randomInt().toLong()
        }

        private fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
        }
    }


}