package joanvila.com.remote

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import joanvila.com.data.model.CharacterEntity
import joanvila.com.data.model.NetworkException
import joanvila.com.remote.factory.CharacterFactory
import joanvila.com.remote.mapper.CharacterRemoteMapper
import joanvila.com.remote.model.BaseResponse
import joanvila.com.remote.model.CharacterResponse
import joanvila.com.remote.utils.ConnectionInterface
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CharactersRemoteImplTest {

    private lateinit var remoteMapper: CharacterRemoteMapper
    private lateinit var charactersService: CharactersService

    private val whenIsConnected = object : ConnectionInterface {
        override fun thereIsConnectivity(): Boolean {
            return true
        }
    }

    private lateinit var charactersRemoteImpl: CharactersRemoteImpl

    @Before
    fun setUp() {
        remoteMapper = mock()
        charactersService = mock()
        charactersRemoteImpl = CharactersRemoteImpl(remoteMapper, charactersService, whenIsConnected)
    }

    @Test
    fun getCharactersCompletes() {
        // Given
        stubCharactersServiceGetCharacters(Single.just(CharacterFactory.createCharactersResponse(2)))
        // When
        val testObserver = charactersRemoteImpl.getCharacters(0).test()
        // Then
        testObserver.assertComplete()
    }

    @Test
    fun getCharactersReturnsData() {
        // Given
        val charactersResponse = CharacterFactory.createCharactersResponse(2)
        stubCharactersServiceGetCharacters(Single.just(charactersResponse))
        val characterEntities = mutableListOf<CharacterEntity>()
        charactersResponse.data?.results?.forEach {
            characterEntities.add(remoteMapper.mapFromRemote(it))
        }
        // When
        val testObserver = charactersRemoteImpl.getCharacters(0).test()
        // Then
        testObserver.assertValue(characterEntities)
    }

    @Test
    fun getCharactersReturnsError() {
        // Given
        val error = Throwable("Unknown error")
        stubCharactersServiceGetCharacters(Single.error(error))
        // When
        val testObserver = charactersRemoteImpl.getCharacters(0).test()
        // Then
        testObserver.assertError(error)
    }

    private fun stubCharactersServiceGetCharacters(observable:
                                                   Single<BaseResponse<CharacterResponse>>) {
        whenever(charactersService.getCharacters())
                .thenReturn(observable)
    }

}