package joanvila.com.remote

class ApiConstants {
    companion object {
        const val BASE_URL = "https://gateway.marvel.com/v1/"
        const val PUBLIC_KEY = "17dc9fe138571caf001f23ed50a0d570"
        const val HASH = "b2bc68200cd46a342048a5bef1fb400e"
        const val TS = "1"
        const val PAGINATION_OFFSET = "offset"
    }
}