package joanvila.com.remote.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

class ConnectionChecker @Inject constructor(val application: Application): ConnectionInterface {

    override fun thereIsConnectivity(): Boolean {
        val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager?.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}