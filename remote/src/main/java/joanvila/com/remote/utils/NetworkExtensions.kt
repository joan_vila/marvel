package joanvila.com.remote.utils

import com.google.gson.Gson
import okhttp3.ResponseBody

fun ResponseBody.extractErrorMessage() : String? {
    try {
        val errorResponse = Gson().fromJson(this.string(), CommonErrorResponse::class.java)
            return errorResponse.error.status
    } catch (e: Exception) {
        return e.localizedMessage
    }

}
