package joanvila.com.remote.utils

import joanvila.com.data.model.NetworkException
import retrofit2.HttpException

object ErrorProcessor {
    fun process(exception: Throwable): Throwable {
        if (exception is HttpException) {
            return when (exception.code()) {
                401, 403 -> NetworkException.UnauthorizedException()
                400, 422, 404 -> NetworkException.UnprocessableEntityException(exception.response().errorBody()?.extractErrorMessage())
                500 -> NetworkException.ServerException()
                else -> Exception()

            }
        }
        return exception
    }
}