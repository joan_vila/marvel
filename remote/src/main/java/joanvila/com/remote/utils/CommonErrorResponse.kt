package joanvila.com.remote.utils

data class CommonErrorResponse(
        val error: Error
)

data class Error(val code: Int,
                 val status: String)