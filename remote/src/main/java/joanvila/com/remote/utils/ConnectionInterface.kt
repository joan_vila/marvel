package joanvila.com.remote.utils

interface ConnectionInterface {
    fun thereIsConnectivity(): Boolean
}