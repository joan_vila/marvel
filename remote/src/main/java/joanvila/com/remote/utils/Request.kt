package joanvila.com.remote.utils

import io.reactivex.Single
import joanvila.com.data.model.NetworkException

sealed class Request<out RxSource> constructor(val connectionChecker: ConnectionInterface) {

    abstract fun buildRequest(): RxSource

    class MakeSingle<T> (connectionChecker: ConnectionInterface, private val request: Single<T>)
        : Request<Single<T>>(connectionChecker) {

        override fun buildRequest() : Single<T> {
            if (connectionChecker.thereIsConnectivity()) {
                return request.onErrorResumeNext {
                    Single.error(ErrorProcessor.process((it)))
                }
            }
            return Single.error(NetworkException.NoInternetConnection())
        }
    }

}