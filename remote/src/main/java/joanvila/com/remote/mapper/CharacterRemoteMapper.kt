package joanvila.com.remote.mapper

import joanvila.com.data.model.CharacterEntity
import joanvila.com.remote.model.CharacterRemoteEntity
import javax.inject.Inject

open class CharacterRemoteMapper @Inject constructor(): Mapper<CharacterRemoteEntity, CharacterEntity> {

    override fun mapFromRemote(type: CharacterRemoteEntity): CharacterEntity {
        return CharacterEntity(
                type.id,
                type.name,
                type.description,
                "${type.characterThumbnail.path}.${type.characterThumbnail.extension}",
                false
        )
    }

}