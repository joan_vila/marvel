package joanvila.com.remote.mapper

interface Mapper<in M, out E> {

    fun mapFromRemote(type: M): E

}