package joanvila.com.remote

import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object CharactersServiceFactory {

    const val TIMEOUT = 10L
    const val TS_QUERY = "ts"
    const val API_KEY_QUERY = "apikey"
    const val HASH_QUERY = "hash"

    fun createCharactersService(): CharactersService {
        val okHttpClient = createOkHttpClient(createRequestInterceptor())
        return createCharactersService(okHttpClient)
    }

    private fun createCharactersService(okHttpClient: OkHttpClient): CharactersService {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl(ApiConstants.BASE_URL)
                .client(okHttpClient)
                .build()
        return retrofit.create(CharactersService::class.java)
    }

    private fun createOkHttpClient(interceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
    }

    fun createRequestInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter(TS_QUERY, ApiConstants.TS)
                    .addQueryParameter(API_KEY_QUERY, ApiConstants.PUBLIC_KEY)
                    .addQueryParameter(HASH_QUERY, ApiConstants.HASH)
                    .build()

            val requestBuilder = original.newBuilder()
                    .url(url)

            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }

}