package joanvila.com.remote

import io.reactivex.Single
import joanvila.com.data.model.CharacterEntity
import joanvila.com.data.repository.CharactersRemote
import joanvila.com.remote.mapper.CharacterRemoteMapper
import joanvila.com.remote.utils.ConnectionInterface
import joanvila.com.remote.utils.Request
import javax.inject.Inject

class CharactersRemoteImpl @Inject constructor(
        private val mapper: CharacterRemoteMapper,
        private val charactersService: CharactersService,
        private val connectionChecker: ConnectionInterface): CharactersRemote {

    override fun getCharacters(offset: Int): Single<List<CharacterEntity>> {
        return Request.MakeSingle(connectionChecker, charactersService.getCharacters(offset).map {
            if (it.data != null) {
                it.data.results.map { mapper.mapFromRemote(it) }
            } else {
                emptyList()
            }
        }).buildRequest()
    }

}