package joanvila.com.remote

import io.reactivex.Single
import joanvila.com.remote.model.BaseResponse
import joanvila.com.remote.model.CharacterResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CharactersService {
    @GET("public/characters")
    fun getCharacters(@Query(ApiConstants.PAGINATION_OFFSET) offset: Int? = 0): Single<BaseResponse<CharacterResponse>>
}