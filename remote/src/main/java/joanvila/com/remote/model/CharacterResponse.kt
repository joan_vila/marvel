package joanvila.com.remote.model

data class CharacterResponse (
        val limit: Int,
        val total: Int,
        val results: List<CharacterRemoteEntity>
)