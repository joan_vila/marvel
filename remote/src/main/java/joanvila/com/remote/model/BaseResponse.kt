package joanvila.com.remote.model

data class BaseResponse<T> (
    val code: Int = 0,
    val status: String? = null,
    val data: T? = null
)