package joanvila.com.remote.model

import com.google.gson.annotations.SerializedName

data class CharacterRemoteEntity(
        val id: Long,
        val name: String,
        val description: String,
        @SerializedName("thumbnail")
        val characterThumbnail: CharacterThumbnail
)

data class CharacterThumbnail (
        val path: String,
        val extension: String
)